package main

import (
	"github.com/gorilla/mux"
	"router/internal/handlers"
	"net/http"
	"fmt"
)

func main() {

	port := "8080"

	router := mux.NewRouter()

	router.HandleFunc("/", handlers.Greetings)

	fmt.Println("Starting server on port: " + port)

	http.ListenAndServe(":" + port, router)
}